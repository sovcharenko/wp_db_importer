<?php

/**
 *  Default variables for the importer.
 *  You can change those variables for permanent use on your server
 */

$default_vars = [
    'db_name' => '',                                                // database name
    'db_user' => '',                                                // database password
    'db_pass' => '',                                                // database password (default empty)
    'db_host' => '',                                                // database host
    'db_port' => '3306',                                            // database port (default - 3306)
    'db_file_path' => $_SERVER['DOCUMENT_ROOT'] . '/',              // database file path
    'url_old' => '',                                                // old blog URL
    'url_new' => $_SERVER['SERVER_NAME'],                           // new blog URL (default - $_SERVER['SERVER_NAME'])
    'root_old' => '',                                               // old root
    'root_new' => $_SERVER['DOCUMENT_ROOT'],                        // new root (default - $_SERVER['DOCUMENT_ROOT'])

    'importer_dir' => '',                                           // importer directory for dumps

    // generate new hashes with md5()
    'importer_hash' => '',                                          // importer password hash (default password - 8524567913)
    'importer_user_hash' => '',                                     // importer username hash (default login - root)

    'save_imported_dumps' => false,                                 // save dumps, uploaded by user
    'save_imported_updated_dumps' => false,                         // save dumps, updated by DBImporter
];

/**
 *  Database Importer init
 */
(new DBImporter($default_vars))->init( $_REQUEST['action'] );

/**
 * Class DBImporter
 * ---------------------------------------------------------------------------------------------------------------------
 */

class DBImporter {

    /**
     * @var array - log
     */
    private $importer_log = [];

    /**
     * @var string - DBImporter directory
     */
    private $importer_dir = '';

    /**
     * @var string - Authorization password hash
     */
    private $importer_hash = '';

    /**
     * @var string - Authorization login hash
     */
    private $importer_user_hash = '';

    /**
     * @var array - fields validation errors
     */
    private $importer_validation_errors = [];

    /**
     * @var array - default variables
     */
    private $importer_defaults = [];

    /**
     * DBImporter constructor.
     * @param array $d_vars - default vars
     */
    function __construct( $d_vars = [] )
    {
        session_start();
        $this->importer_log[] = 'DBImporter Start';
        $this->importer_defaults = $d_vars;
        $this->setVars();
    }


    /**
     *  Logic
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param string $action - current page/action
     */
    function init( $action = '' )
    {
        if( $this->isLoggedIn() ) {
            if( $action ) {
                $action = 'action' . ucfirst($action);
                if( method_exists($this, $action) ) {
                    $this->$action();
                } else {
                    $this->action404();
                }
            } else {
                $this->actionIndex();
            }
        } else {
            $this->actionLogIn();
        }

        die;
    }

    /**
     *  Sets system defaults from $default_vars
     */
    private function setVars()
    {
        if( is_array($this->importer_defaults) ) {

            $this->importer_dir = $this->importer_defaults['importer_dir'] ? $this->importer_defaults['importer_dir'] . '/'
                : 'importer_dir/';
            $this->importer_hash = $this->importer_defaults['importer_hash'] ? $this->importer_defaults['importer_hash']
                : 'd92e96090146a28aa14f14f826b44e65';
            $this->importer_user_hash = $this->importer_defaults['importer_user_hash'] ? $this->importer_defaults['importer_user_hash']
                : '63a9f0ea7bb98050796b649e85481845';

            $this->importer_log[] = 'Default variables have been set successfully';
        }
    }

    /**
     *  Set Defaults from $default_vars
     */
    private function setPostVars()
    {
        if( is_array($this->importer_defaults) ) {
            if( $this->isLoggedIn() ) {
                $_POST['db_name'] = $_POST['db_name'] ? $_POST['db_name'] : $this->importer_defaults['db_name'];
                $_POST['db_user'] = $_POST['db_user'] ? $_POST['db_user'] : $this->importer_defaults['db_user'];
                $_POST['db_pass'] = $_POST['db_pass'] ? $_POST['db_pass'] : $this->importer_defaults['db_pass'];
                $_POST['db_host'] = $_POST['db_host'] ? $_POST['db_host'] : $this->importer_defaults['db_host'];
                $_POST['db_port'] = $_POST['db_port'] ? $_POST['db_port'] : $this->importer_defaults['db_port'];
                $_POST['db_file_path'] = $_POST['db_file_path'] ? $_POST['db_file_path'] : $this->importer_defaults['db_file_path'];
                $_POST['url_old'] = $_POST['url_old'] ? $_POST['url_old'] : $this->importer_defaults['url_old'];
                $_POST['url_new'] = $_POST['url_new'] ? $_POST['url_new'] : $this->importer_defaults['url_new'];
                $_POST['root_old'] = $_POST['root_old'] ? $_POST['root_old'] : $this->importer_defaults['root_old'];
                $_POST['root_new'] = $_POST['root_new'] ? $_POST['root_new'] : $this->importer_defaults['root_new'];
            }
        }
    }

    /**
     * @param array $fields - $_POST array for example
     * @return array - cleared
     */
    private function clearFields( $fields = [] )
    {
        foreach( $fields as $field ) {
            $field = trim( stripslashes( htmlspecialchars($field) ) );
        }
        return $fields;
    }

    /**
     * @param array $fields = [ // fields to validate, sample below
     *                          [$value_required, $name, $no_whitespaces, $condition],
     *                          [$_POST['login'], 'Login', 1, strlen( $_POST['password'] ) > 4 ],
     *                          [ ... ],
     *                       ]
     * @return bool - TRUE if validation passed, FALSE if validation failed
     */
    private function validateFields( $fields = [] )
    {
        $has_errors = false;
        foreach($fields as $i => $field) {
            if( empty( $field[0] ) && !isset($field[3]) ) {
                $has_errors = true;
                $this->importer_validation_errors[] = $field[1] ? ucfirst( $field[1] ) . ' is empty' : 'There is an empty field';
            }
            if( $field[2] && strpos( $field[0], ' ' ) !== false ) {
                $has_errors = true;
                $this->importer_validation_errors[] = $field[1] ? ucfirst( $field[1] ) . ' contains white space(s)' :
                    'Field contains white spaces';
            }
            if( empty( $field[0] ) && isset($field[3]) && $field[3] == true ) {
                $has_errors = true;
                $this->importer_validation_errors[] = $field[1] ? ucfirst( $field[1] ) . ' field error. Check conditions' :
                    'There is an error on some conditional fields';
            }
        }
        return (!$has_errors);
    }

    /**
     * @return string - imploded validation errors
     */
    private function getValidationErrors() {
        return implode( '</br>', $this->importer_validation_errors );
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool - TRUE if authorized, FALSE if not
     */
    private function logIn( $login = '', $password = '' )
    {
        if( md5( $password ) == $this->importer_hash && md5( $login ) == $this->importer_user_hash ) {
            $_SESSION['DBImporter_hash'] = md5($this->importer_hash . $this->importer_user_hash);
            return true;
        } else {
            $this->importer_validation_errors[] = 'Wrong login or password';
            return false;
        }
    }

    /**
     * LogOut (Destroying session variable)
     */
    private function actionLogOut()
    {
        $_SESSION['DBImporter_hash'] = '';
        unset($_SESSION['DBImporter_hash']);
        $this->actionLogIn();
    }

    /**
     * @return bool - TRUE if is logged in, FALSE if not
     */
    private function isLoggedIn()
    {
        if( $_SESSION['DBImporter_hash'] == md5($this->importer_hash . $this->importer_user_hash) ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return string - imploded to html application log
     */
    private function getLog()
    {
        $log_html = '<h4><strong>DBImporter Log:</strong></h4>';
        foreach($this->importer_log as $str) {
            $log_html .= "$str</br>";
        }
        return $log_html;
    }

    /**
     * Creates dump file on server, using user's file or a file on the server
     *
     * @param string $type - 'server' or 'client' file
     * @return bool|string - FALSE when operations failed and a string $filename when success
     */
    private function createDump( $type = '' )
    {
        if( !$type ) {
            $this->importer_log[] = 'File type not defined';
            return false;
        } else {
            $this->importer_log[] = 'Defined file type - ' . $type;
        }

        $dir = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->importer_dir;
        $file_original = '';
        $file_updated = '';

        if( $type == 'client' ) {
            if( $_FILES['db_file']['name'] ) {
                if( strtolower( pathinfo($_FILES['db_file']['name'])['extension'] ) != 'sql' ) {
                    $this->importer_log[] = 'It is not the SQL file [' . pathinfo($_FILES['db_file']['name'])['extension'] . ']';
                    return false;
                }
                $uploadfile = $dir . 'dump_' . mktime() . '_' . basename($_FILES['db_file']['name']);
                if( is_uploaded_file($_FILES['db_file']['tmp_name']) ) {
                    if ( move_uploaded_file($_FILES['db_file']['tmp_name'], $uploadfile) ) {
                        $this->importer_log[] = 'File ' . $_FILES['db_file']['tmp_name']
                            . ' uploaded successfully to ' . $uploadfile;
                        $file_original = $uploadfile;
                    } else {
                        $this->importer_log[] = 'Error saving file';
                        return false;
                    }
                } else {
                    $this->importer_log[] = 'Error uploading file';
                    return false;
                }
            } else {
                $this->importer_log[] = 'Error - file missed';
                return false;
            }
        } elseif( $type = 'server' ) {
            if( $_POST['db_file_path'] ) {
                if( file_exists($_POST['db_file_path']) ) {
                    $this->importer_log[] = 'File found on the server - ' . $_POST['db_file_path'];
                    $file_original = $_POST['db_file_path'];
                } else {
                    $this->importer_log[] = 'Error - file not exist';
                    return false;
                }
            } else {
                $this->importer_log[] = 'Error - file path missed';
                return false;
            }
        }

        if( ($sql_str = file_get_contents($file_original)) !== false ) {

            $this->importer_log[] = 'File data loaded';
            $new_url = $_POST['url_new'] ? $_POST['url_new'] : $_SERVER['SERVER_NAME'];
            $sql_str = str_replace( '//' . $_POST['url_old'], '//' . $new_url, $sql_str );

            $new_root = $_POST['root_new'] ? $_POST['root_new'] : $_SERVER['DOCUMENT_ROOT'];
            $sql_str = str_replace( $_POST['root_old'], $new_root, $sql_str );
            $file_updated = $dir . 'updated_dump_' . mktime() . '.sql';
            $this->importer_log[] = 'File data replaced';

            if( !file_exists($dir) ) {
                $this->importer_log[] = 'Directory does not exist, creating a new one';
                if( mkdir($dir) === true ) {
                    $this->importer_log[] = 'New directory - ' . $dir. ' - created';
                } else {
                    $this->importer_log[] = 'Error directory creating';
                    return false;
                }
            } else {
                $this->importer_log[] = 'Importer directory already exists';
            }
            if( !file_exists( $dir . '.htaccess' ) ) {
                $this->importer_log[] = '.htaccess not found there';
                if( file_put_contents($dir . '.htaccess', 'Options -Indexes') ) {
                    $this->importer_log[] = '.htacces created in directory (for sequrity)';
                } else {
                    $this->importer_log[] = 'Can not create .htacces in directory - something wrong';
                }
            } else {
                $this->importer_log[] = '.htaccess found on directory - good';
            }

            if ( file_put_contents( $file_updated, $sql_str ) !== false ) {
                $this->importer_log[] = 'New temporary file created - ' . $file_updated;
                if( $type == 'client' && $this->importer_defaults['save_imported_dumps'] ) {
                    unlink($file_original);
                }
                return $file_updated;
            } else {
                $this->importer_log[] = 'Error file creating';
                return false;
            }

        } else {
            $this->importer_log[] = 'Error - file not exist';
            return false;
        }
    }

    /**
     * Imports database from file
     *
     * @param string $dump_file - path to DB dump on the server
     * @return bool - operation success or fail
     */
    private function importDataBase( $dump_file = '' )
    {
        $this->fuckSafeMode();

        $mysqli = new mysqli(
            $_POST['db_host'] . ($_POST['db_port'] ? ':' . $_POST['db_port'] : ':3306'),
            $_POST['db_user'],
            $_POST['db_pass'],
            $_POST['db_name']
        );

        if( mysqli_connect_errno() ) {
            $this->importer_log[] = 'DB connection failed';
            return false;
        } else {
            $this->importer_log[] = 'MySQLi connected to the database';
        }

        if( ($query = file_get_contents($dump_file)) !== false) {
            $this->importer_log[] = 'Dump file data recieved';
        } else {
            $this->importer_log[] = 'Dump file opening error';
            $mysqli->close();
            return false;
        }

        $mysqli->query('SET foreign_key_checks = 0');
        if ( $result = $mysqli->query("SHOW TABLES") ) {
            $tables_num = 0;
            while($row = $result->fetch_array(MYSQLI_NUM)) {
                if( $mysqli->query('DROP TABLE IF EXISTS ' . $row[0]) ) {
                    $tables_num++;
                } else {
                    $this->importer_log[] = 'Cannot DROP table - ' . $row[0];
                }
            }
            $this->importer_log[] = 'All ' . $tables_num . ' tables dropped';
        } else {
            $this->importer_log[] = 'Cannot SHOW tables - something wrong';
        }
        $mysqli->query('SET foreign_key_checks = 1');

        $query_num = 0;
        if ($mysqli->multi_query($query)) {
            $this->importer_log[] = 'Multi query executed';
            do { $query_num++; } while ($mysqli->next_result());
            $this->importer_log[] = $query_num . ' queries executed';
        } else {
            $this->importer_log[] = 'Multi query execution Error';
            $mysqli->close();
            return false;
        }

        $mysqli->close();
        $this->importer_log[] = 'DB connection closed';

        return true;
    }

    /**
     * Prevents some errors during import
     */
    private function fuckSafeMode() {
        ini_set('memory_limit', '5000M');
        ini_set("max_execution_time", '5000');
        ini_set("max_input_time", '5000');
        ini_set('default_socket_timeout', '5000');
        @set_time_limit(0);

        $this->importer_log[] = 'PHP safety suffer';
    }

    /**
     *  Rendering / Logic helpers
     * -----------------------------------------------------------------------------------------------------------------
     */

    /**
     *  Log In form rendering
     */
    private function actionLogIn()
    {
        $all_fine = $this->doLogin( $_POST['dbi_login'], $_POST['dbi_password'] );

        ob_start();
        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>DBI LogIn</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?= $this->getStyles(); ?>
        </head>
        <body>
        <div class="container absolute">
            <div class="import-form">
                <h2>Log In</h2>
                <?= $all_fine ? '' : '<h5 class="text-red">' . $this->getValidationErrors() . '</h5>' ?>
                <form action="" method="post" enctype="multipart/form-data" novalidate>
                    <table>
                        <tr>
                            <td><input type="text" id="dbi_login" name="dbi_login" value="<?= $_POST['dbi_login'] ?>" placeholder="Login name" required></td>
                        </tr>
                        <tr>
                            <td><input type="password" id="dbi_password" name="dbi_password" value="<?= $_POST['dbi_password'] ?>" placeholder="Password" required></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="hidden" name="action" value="logIn"><button> Log In </button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <?= $this->getClientScripts(); ?>
        </body>
        </html>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        echo $html;
    }

    /**
     *  LogIn form helper
     *
     * @param string $login
     * @param string $password
     * @return bool - TRUE if logged in successfully, FALSE if not
     */
    private function doLogin( $login = '', $password = '') {
        if( $this->isLoggedIn() ) {
            $this->actionIndex();
            die;
        }

        $all_fine = true;
        if( !empty($_POST) ) {
            $_POST = $this->clearFields($_POST);
            $all_fine = $this->validateFields([
                [ $login, 'Login name' ],
                [ $password, 'Password' ],
            ]);
            if( $all_fine ) {
                $all_fine = $this->logIn( $login, $password );
                if( $all_fine ) {
                    $this->actionIndex();
                    die;
                }
            }
        }

        return $all_fine;
    }

    /**
     * Index page (import form) rendering
     */
    private function actionIndex()
    {
        $this->setPostVars();
        ob_start();
        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>DB Importer</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?= $this->getStyles(); ?>
        </head>
        <body>
        <div class="container">
            <div class="import-form">
                <h2>Import DB</h2>
                <h5 class="text-center"><a href="?action=logOut">Log Out</a></h5>
                <?= $this->getValidationErrors() ? '<h5 class="text-red">' . $this->getValidationErrors() . '</h5>' : '' ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <td><label for="db_name"><span class="text-red">* </span>Database name</label></td>
                            <td><input type="text" id="db_name" name="db_name" value="<?= $_POST['db_name'] ?>" placeholder="Enter DB name" required></td>
                        </tr>
                        <tr>
                            <td><label for="db_user"><span class="text-red">* </span>Database user</label></td>
                            <td><input type="text" id="db_user" name="db_user" value="<?= $_POST['db_user'] ?>" placeholder="root" required></td>
                        </tr>
                        <tr>
                            <td><label for="db_pass">Database password</label></td>
                            <td><input type="text" id="db_pass" name="db_pass" value="<?= $_POST['db_pass'] ?>" placeholder="root"></td>
                        </tr>
                        <tr>
                            <td><label for="db_host"><span class="text-red">* </span>Database remote host address</label></td>
                            <td><input type="text" id="db_host" name="db_host" value="<?= $_POST['db_host'] ?>" placeholder="localhost" required></td>
                        </tr>
                        <tr>
                            <td><label for="db_port">Database port</label></td>
                            <td><input type="text" id="db_port" name="db_port" value="<?= $_POST['db_port'] ?>" placeholder="3306"></td>
                        </tr>
                        <tr>
                            <td><label for="url_old"><span class="text-red">* </span>Previous website domain</label></td>
                            <td><input type="text" id="url_old" name="url_old" value="<?= $_POST['url_old'] ?>" placeholder="localhost.com" required></td>
                        </tr>
                        <tr>
                            <td><label for="url_new">New website domain</label></td>
                            <td><input type="text" id="url_new" name="url_new" value="<?= $_POST['url_new'] ?>" placeholder="www.example.com" required></td>
                        </tr>
                        <tr>
                            <td><label for="root_old"><span class="text-red">* </span>Previous document root</label></td>
                            <td><input type="text" id="root_old" name="root_old" value="<?= $_POST['root_old'] ?>" placeholder="C:/localserver/localhost" required></td>
                        </tr>
                        <tr>
                            <td><label for="root_new">New document root</label></td>
                            <td><input type="text" id="root_new" name="root_new" value="<?= $_POST['root_new'] ?>" placeholder="/home/usr/jkfdlsiiisfd/public_html" required></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="db_file">
                                    <input type="radio" checked name="db_file_option" id="db_file_option_1"  value="client">
                                    <label for="db_file_option_1">SQL dump file download</label>
                                </label>
                            </td>
                            <td><input type="file" id="db_file" name="db_file"></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="db_file_path">
                                    <input type="radio" name="db_file_option" id="db_file_option_2" value="server">
                                    <label for="db_file_option_2">Path to SQL dump file on the server</label>
                                </label>
                            </td>
                            <td><input type="text" id="db_file_path" name="db_file_path" value="<?= $_POST['db_file_path'] ?>" placeholder=""></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="hidden" name="action" value="import"><button> Import DB </button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <?= $this->getClientScripts(); ?>
        </body>
        </html>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        echo $html;
    }

    /**
     * 404 - action not found
     */
    private function action404()
    {
        ob_start();
        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>DB Importer</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?= $this->getStyles(); ?>
        </head>
        <body>
        <div class="container absolute text-center">
            <h1>404</h1>
            <h4 class="text-red"> undefined action</h4>
            <h5><a href="/">Back</a></h5>
        </div>
        <?= $this->getClientScripts(); ?>
        </body>
        </html>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        echo $html;
    }

    /**
     * Import rendering
     */
    private function actionImport()
    {
        $this->doImport();

        ob_start();
        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>DB Importer</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <?= $this->getStyles(); ?>
        </head>
        <body>
        <div class="container">
            <div class="import-form">
                <h3 class="text-center">Processing log</h3>
                <ul>
                    <li><h5><a href="/">Back</a></h5></li>
                    <li><h5><a href="?action=logOut">LogOut</a></h5></li>
                </ul>
                <?= $this->getLog() ?>
            </div>
        </div>
        <?= $this->getClientScripts(); ?>
        </body>
        </html>
        <?
        $html = ob_get_contents();
        ob_end_clean();

        echo $html;
    }

    /**
     *  Import helper
     */
    private function doImport() {
        $_POST = $this->clearFields($_POST);
        if( !$this->validateFields([
            [$_POST['db_name'], 'Database Name', 1],
            [$_POST['db_user'], 'Database User', 1],
            [$_POST['db_host'], 'Database Host', 1],
            [$_POST['url_old'], 'URL old', 1],
            [$_POST['root_old'], 'ROOT old', 1],
            [$_POST['db_file_path'], 'Server file path', 0, $_POST['db_file_option'] == 'server'],
            [$_FILES['db_file']['name'], 'Client file', 0, $_POST['db_file_option'] == 'client'],
        ]) ) {
            $this->actionIndex();
            die;
        }

        $create_result = $this->createDump( $_POST['db_file_option'] );

        if( $create_result !== false ) {

            $import_result = $this->importDataBase($create_result);
            if( $import_result ) {
                $this->importer_log[] = 'DB IMPORT SUCCESSFULL';
            } else {
                $this->importer_log[] = 'DATABASE PROCESSING FAILED';
            }

            if( $this->importer_defaults['save_imported_updated_dumps'] ) {
                unlink($create_result);
                $this->importer_log[] = 'Temporary file deleted';
            }

        } else {
            $this->importer_log[] = 'DUMP FILE PROCESSING FAILED';
        }
    }

    /**
     * @return string - built-in CSS styles for all pages
     */
    private function getStyles()
    {
        ob_start();
        ?>
        <style type="text/css">
            html *{margin:0;padding:0;font-family:Verdana, Geneva, sans-serif}a{color:#757575}.absolute{position:absolute;top:0;bottom:0;left:0;right:0}.text-center{text-align:center}.text-red{color:red}.import-form h2{text-align:center;margin:10px 0 25px 0;text-transform:uppercase}.import-form{margin:0 auto;width:60%}.import-form table{width:100%}.import-form table td{padding:5px;width:50%}.import-form input:not([type=radio]){width:95%;height:100%;padding:7px;border-radius:5px;border:none;outline:none;box-shadow:inset 0 0 7px 1px #757575}.import-form input:not([type=radio]):focus{box-shadow:0 0 7px 1px #757575}.import-form button{text-transform:uppercase;font-weight:bold;display:block;letter-spacing:2px;padding:7px 20px;margin:10px auto;border-radius:5px;border:none;outline:none;box-shadow:0 0 7px 1px #757575;-o-transition:0.3s;-webkit-transition:0.3s;-moz-transition:0.3s;transition:0.3s}.import-form button:hover{cursor:pointer;background-color:springgreen}@media screen and (max-width: 800px){.import-form{margin:0 auto;width:90%}}@media screen and (max-width: 500px){.import-form table td{display:block;width:100%}}
        </style>
        <?
        $CSS = ob_get_contents();
        ob_end_clean();

        return $CSS;
    }

    /**
     * @return string - built-in JS for all pages
     */
    private function getClientScripts()
    {
        ob_start();
        ?>
        <script type="text/javascript">
            // @TODO client validation
        </script>
        <?
        $JS = ob_get_contents();
        ob_end_clean();

        return $JS;
    }
}